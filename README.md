# My arch setup scripts

this repository is a collection of scripts config files and dependency lists to make installing arch easyer

## install arch to hardrive
install git, pull repo and run install script
 > pacman -Sy && pacman -S --noconfirm git && git clone https://gitlab.com/calvinreu/arch-install && cd arch-install && ./install.sh

NOTE: to use kbdmod the conf should be in /tmp/interception

## additional setup
the other script files do some sort of setup I use install_default.sh to run them all and install some additional software it should be easy to understand the scripts since their very short and easy to modify them

run to execute my default installation this has to be done after a reboot otherwise pamac will not work properly due to btrfs
 > cd /tmp && git clone https://gitlab.com/calvinreu/arch-install && cd arch-install && ./install_default.sh USERNAME