#!/bin/bash
echo "changing shell to fish"

getent passwd {1000..6000} | awk -F: '{ print $1}'

# Setting fish as shell
chsh -s "/bin/fish" root
for i in $1
do
chsh -s "/bin/fish" $i
done