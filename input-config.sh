#! /bin/bash

# system keyboard layout
read -r -p "keyboard layout? when using multiple enter them comma seperated "
sed -i "s/#export XKB_DEFAULT_LAYOUT=/export XKB_DEFAULT_LAYOUT=$REPLY/g" /usr/share/sddm/scripts/wayland-session
localectl set-x11-keymap "$REPLY"

read -r -p "keyboard options file? "
# keyboard xkb options
sed -i "s/#export XKB_DEFAULT_OPTIONS=/export XKB_DEFAULT_OPTIONS=$(cat config\/$REPLY)/g" /usr/share/sddm/scripts/wayland-session
