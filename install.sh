#! /usr/bin/env bash

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# warn about required setup partiontable
echo "WARNING: if you intend to not wipe your disk, you need to manually partition it first a efi, boot and primary partition are required"

# ask if user wants to continue
read -r -p "Do you want to continue? [y/n] "
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 0
fi

clear

# ask for layout
read -r -p "Please enter your layout (e.g. us): " layout
loadkeys "$layout"

# wifi using iwctl
# ask if wifi is needed
read -r -p "connect to wifi? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
    #scan
    iwctl station wlan0 scan
    # list networks and select one
    select ssid in $(iwctl station wlan0 get-networks | grep SSID | cut -d " " -f2-)
    do
        break
    done


    # ask for password
    read -r -p "password? " password
    # connect to wifi
    iwctl station wlan0 connect "$ssid" password "$password"
fi

#ensure internet connection
ping -c 1 archlinux.org || exit 1
# reflector
reflector --verbose --latest 5 --sort rate --save /etc/pacman.d/mirrorlist

# ask for kbdmod
read -r -p "do you want to use kbdmod? [y/N] "
if [[ $REPLY =~ ^[Yy]$ ]]
then
    kbdmod="kbdmod"
else
    kbdmod=""
fi

# get necessary information
read -r -p "username? " username
read -r -p "password? " userpassword
read -r -p "repeat password? " userpassword2
# check if passwords match
if [[ "$userpassword" != "$userpassword2" ]]
then
    echo "passwords do not match"
    exit 1
fi

#ask for encryption password
read -r -p "encryption password? " encpassword
read -r -p "repeat encryption password? " encpassword2
# check if passwords match
if [[ "$encpassword" != "$encpassword2" ]]
then
    echo "passwords do not match"
    exit 1
fi

read -r -p "hostname? " hostname

#echo time zone for reference
echo "timezone format: Area/City for example Europe/Berlin if empty your IP will be used to determine your timezone"
read -r -p "timezone? " timezone

#ask for locale gen language
echo "language: for example en_US"
read -r -p "language? " language
language="$language:.UTF-8"


# ask for locale format
echo "locale format: for example en_US"
read -r -p "locale? " locale
locale="$locale.UTF-8 UTF-8"

# Selecting the kernel flavor to install. 
echo "List of kernels:"
echo "1) Stable — Vanilla Linux kernel and modules, with a few patches applied."
echo "2) Hardened — A security-focused Linux kernel."
echo "3) Longterm — Long-term support (LTS) Linux kernel and modules."
echo "4) Zen Kernel — Optimized for desktop usage."
read -r -p "Insert the number of the corresponding kernel: " choice
case $choice in
    1 ) kernel=linux
        ;;
    2 ) kernel=linux-hardened
        ;;
    3 ) kernel=linux-lts
        ;;
    4 ) kernel=linux-zen
        ;;
    * ) echo "You did not enter a valid selection."
        kernel_selector
esac
echo "kernel: $kernel"

#ask for random MAC
read -r -p "randomize MAC? [y/N] " randommac

# Selecting the target Disk for the installation.
echo "Select the disk where Arch Linux is going to be installed: "
select ENTRY in $(lsblk -dpnoNAME|grep -P "/dev/sd|nvme|vd");
do
    DISK=$ENTRY
    break
done

# Ask to wipe disk
read -r -p "wipe disk else manually assign partitions? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
    # warn about wiping disk
    echo "WARNING: this will wipe your disk"
    # ask if user wants to continue
    read -p "Do you want to continue? [y/n] " -r
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        exit 0
    fi
    # wipe disk
    wipefs -a "$DISK"
    
    # Creating a new partition scheme.
    echo "Creating new partition scheme on $DISK."
    parted -s "$DISK" \
        mklabel gpt \
        mkpart ESP fat32 1MiB 600MiB \
        set 1 esp on \
        mkpart cryptroot 600MiB 100% \


    sleep 0.1
    ESP="/dev/$(lsblk "$DISK" -o NAME,PARTLABEL | grep ESP | cut -d " " -f1 | cut -c7-)"
    cryptroot="/dev/$(lsblk "$DISK" -o NAME,PARTLABEL | grep cryptroot | cut -d " " -f1 | cut -c7-)"

    # Informing the Kernel of the changes.
    echo "Informing the Kernel about the disk changes."
    partprobe "$DISK"

    mkfs.fat -F32 "$ESP"
else
    # get ESP
    read -r -p "ESP partition? $DISK"
    ESP="$DISK$REPLY"
    # get primary partition
    read -r -p "primary partition? $DISK"
    cryptroot="$DISK$REPLY"

    # warn about wiping disk
    echo "WARNING: this will destroy this partition: $cryptroot"
    # ask if user wants to continue
    read -p "Do you want to continue? [y/n] " -r
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        exit 0
    fi
fi

# Inform about installation beginning
echo "everything is set up, installation will begin now, this will take a while"

# encrypt btrfs partition
echo "encrypting btrfs partition"
echo -n "$encpassword" | cryptsetup luksFormat "$cryptroot"
echo "opening encrypted partition"
echo -n "$encpassword" | cryptsetup open "$cryptroot" luks

# create btrfs volume
echo "creating btrfs volume"
mkfs.btrfs -f /dev/mapper/luks
mount /dev/mapper/luks /mnt

#Creating BTRFS subvolumes
echo "Creating BTRFS subvolumes."
btrfs sub create /mnt/@
btrfs sub create /mnt/@home
btrfs sub create /mnt/@pkg
btrfs sub create /mnt/@snapshots

umount /mnt

# mount btrfs subvolumes
echo "mounting btrfs subvolumes"
mount -o noatime,nodiratime,compress=zstd,space_cache=v2,ssd,subvol=@ /dev/mapper/luks /mnt
mkdir -p /mnt/{boot,home,var/cache/pacman/pkg,.snapshots,btrfs}
mount -o noatime,nodiratime,compress=zstd,space_cache=v2,ssd,subvol=@home /dev/mapper/luks /mnt/home
mount -o noatime,nodiratime,compress=zstd,space_cache=v2,ssd,subvol=@pkg /dev/mapper/luks /mnt/var/cache/pacman/pkg
mount -o noatime,nodiratime,compress=zstd,space_cache=v2,ssd,subvol=@snapshots /dev/mapper/luks /mnt/.snapshots
mount -o noatime,nodiratime,compress=zstd,space_cache=v2,ssd,subvolid=5 /dev/mapper/luks /mnt/btrfs

# mount ESP
echo "mounting ESP"
mount "$ESP" /mnt/boot

# Checking the microcode to install.
CPU=$(grep vendor_id /proc/cpuinfo)
if [[ $CPU == *"AuthenticAMD"* ]]; then
    microcode=amd-ucode
else
    microcode=intel-ucode
fi

# Load list of default packages
BASEPACKAGES="$(cat "$SCRIPT_DIR"/dependencies/base_dependencies)"

# Installing Arch Linux.
echo "Installing Arch Linux."
pacstrap /mnt $kernel $BASEPACKAGES

# Install microcode
echo "Installing microcode"
arch-chroot /mnt pacman -S --noconfirm $microcode

# Generating the fstab file.
echo "Generating the fstab file."
genfstab -U /mnt >> /mnt/etc/fstab

# Setting hostname.
echo "Setting hostname."
echo "$hostname" > /mnt/etc/hostname

# Setting timezone.
echo "Setting timezone."
#if timezone is empty geoip
if [ -z "$timezone" ]; then
    arch-chroot /mnt ln -sf /usr/share/zoneinfo/"$(curl -s https://ipapi.co/timezone)" /etc/localtime
else
    arch-chroot /mnt ln -sf "/usr/share/zoneinfo/$timezone" /etc/localtime
fi

arch-chroot /mnt hwclock --systohc
arch-chroot /mnt timedatectl set-ntp true

# Setting language.
echo "Setting language."
echo LANG="$language" > /mnt/etc/locale.conf

# Setting locale.
echo "Setting locale."
echo "$locale" > /mnt/etc/locale.gen
arch-chroot /mnt locale-gen

# Setting hosts
echo "Setting hosts."
cat << EOF > /mnt/etc/hosts
    # Host addresses
    127.0.0.1  localhost
    127.0.1.1  $hostname
    ::1        localhost ip6-localhost ip6-loopback
    ff02::1    ip6-allnodes
    ff02::2    ip6-allrouters
EOF

# set keyboard layout
echo "Setting keyboard layout."
echo "KEYMAP=$layout" > /mnt/etc/vconsole.conf

# get UUID of btrfs partition
UUID=$(blkid -s UUID -o value "$cryptroot")

# check if kbdmod is true
if [ "$kbdmod" = "kbdmod" ]; then
    # build kbdmod as hook
    echo "Building kbdmod."
    # clone kbdmod
    git clone "https://gitlab.com/calvinreu/kbdmod.git" /mnt/tmp/kbdmod
    # compile kbdmod
    cd mnt/tmp/kbdmod || echo "kbdmod not found"
    #install compile dependencies
    arch-chroot /mnt pacman -S --noconfirm --needed cmake gcc make
    arch-chroot /mnt /tmp/kbdmod/install.sh "--DESTDIR=/bin"
    # move to hook install directory
    cp /mnt/tmp/kbdmod/initcpioinstall /mnt/etc/initcpio/install/kbdmod
    # move to hook dir
    cp /mnt/tmp/kbdmod/initcpiohook /mnt/etc/initcpio/hooks/kbdmod
    # copy conf
    cp -r /tmp/interception /mnt/etc/interception || copy_kbdmodconf
fi

function copy_kbdmodconf() {
    # get folder
    echo "kbdmod conf not found"
    read -p "folder?"
    cp -r "$REPLY" /mnt/etc/interception
}

# Replace HOOKS
echo "Replacing HOOKS"
sed -i "s/^HOOKS=.*/HOOKS=(base keyboard udev autodetect modconf block keymap encrypt btrfs filesystems $kbdmod)/" /mnt/etc/mkinitcpio.conf

# Recreate initramfs
echo "Recreating initramfs"
mkdir -p /mnt/boot/loader/entries
arch-chroot /mnt mkinitcpio -P

# Creating bootloader config.
touch /mnt/boot/loader/entries/arch.conf
cat << EOF > /mnt/boot/loader/entries/arch.conf
title Arch Linux
linux /vmlinuz-$kernel
initrd /$microcode.img
initrd /initramfs-$kernel.img
options cryptdevice=UUID=$UUID:luks:allow-discards root=/dev/mapper/luks rootflags=subvol=@ rd.luks.options=discard rw
EOF

# Creating bootloader fallback config.
touch /mnt/boot/loader/entries/arch-fallback.conf
cat << EOF > /mnt/boot/loader/entries/arch-fallback.conf
title Arch Linux Fallback
linux /vmlinuz-$kernel
initrd /$microcode.img
initrd /initramfs-$kernel-fallback.img
options cryptdevice=UUID=$UUID:luks:allow-discards root=/dev/mapper/luks rootflags=subvol=@ rd.luks.options=discard rw
EOF

# Set default boot entry
echo "Setting default boot entry"
touch /mnt/boot/loader/loader.conf
cat << EOF > /mnt/boot/loader/loader.conf
default arch.conf
timeout 2
console-mode max
editor 0
EOF

# Add user
echo "Adding user"
# write var to /mnt
echo "$username:$password" > /mnt/tmp/user
# add user
arch-chroot /mnt /bin/bash -c "useradd -m -G wheel -s /bin/bash $(cat /tmp/user | cut -d: -f1)"
# Set password
arch-chroot /mnt /bin/bash -c "chpasswd < /tmp/user"
# remove tmp file
rm /mnt/tmp/user

# Add sudo
sed -i 's/^# %wheel ALL=(ALL) ALL$/%wheel ALL=(ALL) ALL/' /mnt/etc/sudoers

# Randomize root password
# random string of lenght 32
rootpassword=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
arch-chroot /mnt passwd << EOF
    $rootpassword
    $rootpassword
EOF

# Random MAC address
if [[ "$randommac" =~ ^(yes|y)$ ]]; then
# Randomize Mac Address.
bash -c 'cat > /mnt/etc/NetworkManager/conf.d/00-macrandomize.conf' <<-'EOF'
[device]
wifi.scan-rand-mac-address=yes
[connection]
wifi.cloned-mac-address=random
ethernet.cloned-mac-address=random
connection.stable-id=${CONNECTION}/${BOOT}
EOF

# ZRAM configuration
bash -c 'cat > /mnt/etc/systemd/zram-generator.conf' <<-'EOF'
[zram0]
zram-fraction = 1
max-zram-size = 8192
EOF

chmod 600 /mnt/etc/NetworkManager/conf.d/00-macrandomize.conf
fi

# Disable Connectivity Check.
bash -c 'cat > /mnt/etc/NetworkManager/conf.d/20-connectivity.conf' <<-'EOF'
[connectivity]
uri=http://www.archlinux.org/check_network_status.txt
interval=0
EOF

chmod 600 /mnt/etc/NetworkManager/conf.d/20-connectivity.conf

# Enable IPv6 privacy extensions
bash -c 'cat > /mnt/etc/NetworkManager/conf.d/ip6-privacy.conf' <<-'EOF'
[connection]
ipv6.ip6-privacy=2
EOF

chmod 600 /mnt/etc/NetworkManager/conf.d/ip6-privacy.conf

# Remove nullok from system-auth
sed -i 's/nullok//g' /mnt/etc/pam.d/system-auth

# Disable coredump
echo "* hard core 0" >> /mnt/etc/security/limits.conf

# Disable su for non-wheel users
bash -c 'cat > /mnt/etc/pam.d/su' <<-'EOF'
#%PAM-1.0
auth		sufficient	pam_rootok.so
# Uncomment the following line to implicitly trust users in the "wheel" group.
auth		sufficient	pam_wheel.so trust use_uid
# Uncomment the following line to require a user to be in the "wheel" group.
auth		required	pam_wheel.so use_uid
auth		required	pam_unix.so
account		required	pam_unix.so
session		required	pam_unix.so
EOF

# Enable services
echo "Enabling services"
arch-chroot /mnt /bin/bash -e <<EOF
    systemctl enable NetworkManager
    systemctl enable bluetooth
    systemctl enable fstrim.timer
    systemctl enable reflector.timer
    systemctl enable tlp
    systemctl enable systemd-timesyncd
    systemctl enable apparmor
    systemctl enable firewalld
    systemctl enable org.cups.cupsd.service
    systemctl enable reflector.timer
    systemctl enable sshd
    systemctl enable snapper-timeline.timer
    systemctl enable snapper-cleanup.timer
    systemctl enable snapper-boot.timer
    systemctl enable oomd
    systemctl enable systemd-resolved
    
    # Update mirrorlist
    reflector --verbose --latest 5 --sort rate --save /etc/pacman.d/mirrorlist
    
    # Update system
    pacman -Syu --noconfirm
    
    bootctl --path=/boot install

EOF

# enable udevmon if kbdmod is enabled
if [ "$kbdmod" = "kbdmod" ]; then
    arch-chroot /mnt /bin/bash -e <<EOF
        systemctl enable udevmon
EOF
fi


# Finishing up
echo "Done, you may now wish to reboot (further changes can be done by chrooting into /mnt)."

exit
