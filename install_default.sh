#!/bin/bash

SCRIPT_DIR="$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)"

if [ [ -z $1 ] || [ "$username" == "USERNAME" ] ]; then
    echo require username
    exit
fi

cd "$SCRIPT_DIR"
./change-shell.sh
 
cd "$SCRIPT_DIR"
./install_pamac.sh $1
 
cd "$SCRIPT_DIR"
./install_riverwm.sh $1
 
cd "$SCRIPT_DIR"
./os_probe.sh
 
cd "$SCRIPT_DIR"
./install_packagelist.sh "dependencies/default-setup"
 
cd "$SCRIPT_DIR"
./install_packagelist.sh "dependencies/default-apps.pamac"
 

cd /tmp
git clone https://gitlab.com/calvinreu/riverwm-dotfiles
cd $SCRIPT_DIR
./install_packagelist.sh "/tmp/riverwm-dotfiles/dependencies"*
 
su "$1" -c "/tmp/riverwm-dotfiles/install.sh"



#setup sddm
systemctl enable sddm
cp ./config/sddm.conf /etc/sddm.conf
cp "./config/wayland-session_sddm" "/usr/share/sddm/scripts/wayland-session"
#sed -i "s/#User=/User=$1/g" /etc/sddm.conf
chmod 600 /etc/sddm.conf
chmod 600 /usr/share/sddm/scripts/wayland-session

#keyboard conf
./input-config.sh
