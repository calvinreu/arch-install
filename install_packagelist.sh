#! /bin/bash

function install_packagelist () {
    if [[ "$1" == *".pamac" ]]; then
        pamac install --no-confirm ${2}
    else
        pacman -S --needed --noconfirm ${2}
    fi
}

if [[ -d $1 ]]; then
    PACKAGELIST=""
    files=$(ls $1)
    for file in ${files}
    do
        install_packagelist $file "$(cat "$1$file")"
    done
else
    install_packagelist "$1" "$(cat $1)"
fi

