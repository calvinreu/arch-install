#!/bin/bash

pacman -S --needed git base-devel flatpak --noconfirm
git clone https://aur.archlinux.org/libpamac-flatpak.git
cd libpamac-flatpak
makepkg -si
cd ..
git clone https://aur.archlinux.org/pamac-flatpak.git
cd pamac-flatpak
makepkg -si