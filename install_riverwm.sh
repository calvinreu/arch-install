#! /bin/bash

if [[ -z "$1" ]]; then
    echo "parse username to install config to as first arg"
    exit
fi

SCRIPT_DIR="$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)"

mkdir -p "/home/$username/.config/river/"
cd "/home/$username/.config/river"
wget https://raw.githubusercontent.com/riverwm/river/master/example/init
chmod 775 init

cd "$SCRIPT_DIR"
./install_packagelist.sh "dependencies/riverwm"
su "$1" -c "pamac install river"

usermod -G seat -a "$1"
systemctl enable seatd