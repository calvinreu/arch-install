#!/bin/bash

SCRIPT_DIR="$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)"

cd "$SCRIPT_DIR"

### Install Pamac ###
./install_packagelist.sh "dependencies/pamac"
 
cd /tmp
su "$1" -c "git clone https://aur.archlinux.org/package-query.git"
cd package-query/
su "$1" -c "makepkg --noconfirm"
pacman --noconfirm -U *pkg.tar.*

cd ../
su "$1" -c "git clone https://aur.archlinux.org/yaourt.git"
cd yaourt/
su "$1" -c "makepkg --noconfirm"
pacman --noconfirm -U *pkg.tar.*
su "$1" -c "yaourt -S pamac-aur"
sed -i 's/#EnableAUR/EnableAUR/g' /etc/pamac.conf
sed -i 's/#CheckAURUpdates/CheckAURUpdates/g' /etc/pamac.conf
sed -i 's/#DownloadUpdates/DownloadUpdates/g' /etc/pamac.conf
### End of Pamac install ###



# Install packages
echo "Installing packages"


